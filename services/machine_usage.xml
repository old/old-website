<?xml version='1.0'?>
<!DOCTYPE cscpage SYSTEM "../csc.dtd">

<cscpage title="CSC Machine Policy, 15 February 1995">
    <header/>

<section title="Notes">
<p>
Everyone who receives an account on one of the CSC machines must sign
the agreement in the final section.  This document does not state who
will be allowed accounts on CSC machines, the normal expiry period of
accounts, nor any other similar matters.  Further, this policy does
not, in general, guarantee any ``rights'' to users.
</p>
<p>
Policies for group accounts and administrative accounts are not yet
available (this section will contain links to them when they do become
available).  There is also a brief (instead of legalese)
<a href="machine_usage_summary">summary of the usage policy</a> available.
</p>
<p>
The usage policy signature form is available as an <a
href="/misc/usage_agreement.odt">ODT file</a>.
</p>
</section>

<section title="Usage Policy">

<p>The usage policy is divided into the following sections:</p>
<ol>
<li><a href="#usageaccept">Acceptable and Unacceptable Use</a></li>
<li><a href="#usagerespons">User Responsibilities</a></li>
<li><a href="#usagesecurity">Security</a></li>
<li><a href="#usagerights">Rights of the Systems Committee and the CSC Executive</a></li>
</ol>

<p>
Note that in the following sections, the term &quot;user&quot; implies a user of a
CSC machine, unless otherwise specified.
</p>

<h3><a id="usageaccept">Acceptable and Unacceptable Use</a></h3>

<p>
The CSC machines are intended for research, personal projects,
and general use in accordance with the aims of the CSC (see the
<a href="../about/constitution">CSC Constitution</a> for further details).
Projects that are of interest to the CSC may be given special priority
by the CSC Systems Committee.
</p>

<p>
Users must adhere to the CSC's policies concerning machine usage.
</p>

<h3><a id="usagerespons">User Responsibilities</a></h3>
<p>
Users must be responsible for their behaviour.
Users, and not the CSC, will be held accountable for any of their illegal,
damaging or unethical actions.  Such actions are obviously not permitted
on CSC machines.
</p>
<p>
Users must act responsibly, and the needs of others with regard to
computing resources must be considered at all times. In particular, no
user should use any resource to an extent that other users are prevented
from similarly using that resource, and no user's actions shall disrupt
the work of other users.
</p>
<p>
Users must also abide by the usage policies of all machines that they
connect to from CSC machines, or use to connect to CSC machines. It is
the users' responsibility to check the policies of <em>all</em> machines
that they connect to.

</p>

<h3><a id="usagesecurity">Security</a></h3>
<p>
Users may not attempt to gain access to accounts
other than those which they have been permitted to use.
Similarly, users may not attempt to access other users'
private files, nor may they attempt to find out the password of
any account.
</p>
<p>
An account may only be used by the person assigned to it. <em>Do not
tell your password to anybody, or let anyone else use your account</em>.
Users should consider the security implications of their actions. For
example:
</p>
<ul>
<li>Passwords for accounts on CSC machines should not be used on
      other machines</li>

<li>Accounts not on MFCF or CSC machines should not be granted
      automatic access to CSC accounts (e.g. via .rhosts files).</li>
</ul>

<p>
The appropriate members of the systems committee must be notified immediately
in the event that a security problem is found.  Naturally, the problem
should neither be exploited nor made public until it can be corrected.
</p>

<h3><a id="usagerights">Rights of the Systems Committee and the CSC Executive</a></h3>
<p>
The Systems Committee may examine any files or programs believed to be out of
control or in violation of the usage policies for the CSC network.
Examination of a program includes examination of the running process
and its binary.  Files believed to be the data or source to the process
may also be examined.  The process may be killed, stopped or otherwise
interrupted at the discretion of the Systems Committee.  If
the Systems Committee takes any of the
above actions, the owner of the process will be notified.
</p>
<p>
The Systems Committee may at any time revoke a user's permission to
access an account provided that a written (possibly electronic)
explanation is given.  Cause for removal of access to an account
includes, but is not limited to, violation of the machine usage policy.
In the event of a dispute, a user whose account has been revoked
may appeal to the CSC Executive for its reinstatement, as per the
<a href="../about/constitution">CSC Constitution</a>.

</p>
<p>
The CSC Executive has the right to update any policy, including this one,
with reasonable notice.
</p>

</section>

<section title="Club Accounts">
<p>The club accounts policy is divided into the following 2 sections:</p>
<ol>
<li><a href="#clubaccess">Access Control</a></li>
<li><a href="#clubrespons">Responsibility and Accountability</a></li>
</ol>

<h3><a id="clubaccess">Access Control</a></h3>
<p>
Note:  For the given section, any mention of the club, except in direct 
reference to the Computer Science Club, will refer an club other than the
CSC, which has, or requests, an account on a Computer Science Club machine.
</p>
<p>
Clubs are given accounts to provide them with e-mail and WWW pages, but are
subject to the following to certain rules. They are as follows:
</p>
<ol>
<li>
The club account is subject to all restrictions of a users
	account, except that it is a shareable account.
    </li>
<li>
        The club members must have regular user accounts on the CSC
        machine that the club account will be on. If the club member
        does not already have such an account, one will be created
        to allow the member to manage the club account.
</li>
<li>
        The members of the club with access to the club
        account shall be known to the CSC Systems
        Administrator to ensure that these people are aware of this
        section of the user agreement.
    </li>
<li>
The club members with access to the club account shall not grant 
        access to any other members by any means that are available to
        them, other than approaching the CSC System Administrator and
        requesting the change of access.
    </li>
</ol>

<h3><a id="clubrespons">Responsibility and Accountable</a></h3>
<p>
The account is the responsibility of the members who have access.  If the
resources owned by the club account are found to be in violation of any 
policy/rule/law of any of, but not limited to, the Computer Science Club,
MFCF, the University of Waterloo, or any relevant law enforcement agency,
then the members with access will be held <b>equally</b> responsible for
that action.
</p>
</section>

<section title="Definitions">
<dl>
<dt>CSC</dt>
<dd>The University of Waterloo <a href="/">Computer Science Club</a>.
        The CSC office is located in room
        3036/3037 of Mathematics and Computer Building (UW campus),
        telephone number (519) 888-4657 x3870.
    </dd>

<dt>CSC Network</dt>
<dd>
The network of computer hardware and peripherals owned by,
        rented to, on loan to, or otherwise under the control of the CSC.
    </dd>
<dt>MFCF</dt>
<dd>
The <a href="http://www.math.uwaterloo.ca/mfcf/">Math Faculty
        Computing Facility</a> at the University of Waterloo.
    </dd>

<dt>Machine</dt>
<dd>
Computer, terminal or other piece of hardware.  Non-CSC machines
        include MFCF's xterms and printers.
    </dd>
<dt>Systems Committee</dt>
<dd>
An appointed body responsible for the operation of the CSC network
        and the software that runs on it.
        A complete description is available in the
        <a href="../about/constitution">CSC Constitution</a>.
    </dd>

<dt>Network Bandwidth</dt>
<dd>
The percentage of bytes per unit of time that can be handled by
        the network(s) in question.  These networks include the University
        of Waterloo on-campus network and the Internet.
    </dd>
<dt>Computing Resources</dt>
<dd>Resources the CSC considers limited include
        <ul>
<li>public temporary disk space</li>

<li>swap space</li>
<li>other commonly held disk space
              (which may include the home file system)</li>
<li>inodes</li>
<li>memory</li>
<li>CPU time</li>
<li>processes</li>

<li>ttys and pseudo-ttys</li>
<li>network bandwidth</li>
<li>ports</li>
<li>computer paper</li>
</ul>
</dd>
</dl>
</section>

<section title="User Agreement">
<p>
I have read and understood the usage policy of 29 August 2007, and I
agree to use my account(s) on the CSC network in accordance with this
policy.  I am responsible for all actions taken by anyone using this
account.  Furthermore, I accept full legal responsibility for all of the
actions that I commit using the CSC network according to
any and all applicable laws.
</p>
<p>
I understand that with little or no notice machines on the CSC
network and resources on these machines may become unavailable.
Machines may ``go down'' while users are using them, and I will
not hold the CSC responsible for lost time or data.
</p>

<pre>
Name:         ___________________________

Signature:    ___________________________

Office Staff: ___________________________

Signature:    ___________________________

Date:         ___________________________
</pre>
</section>

<footer />

</cscpage>
