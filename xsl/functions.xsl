<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template name="donate-now">
<p>Please consider <a href="{$g_pre}about/donations">donating to the
Computer Science Club</a> to help offset the costs of bringing you our
talks.</p>
</xsl:template>

<xsl:template name="inline-ads">
<!-- google ads -->
<p>
  <script type="text/javascript">
    google_ad_client = "pub-5235744946475097";
    google_ad_width = 728;
    google_ad_height = 90;
    google_ad_format = "728x90_as";
    google_ad_type = "text_image";
    //2007-03-21: mcc
    google_ad_channel = "1106187834";
  </script>
  <script type="text/javascript"
      src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    <xsl:text> </xsl:text>
  </script>
</p>
</xsl:template>

</xsl:stylesheet>
