<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/2005/Atom"
    exclude-result-prefixes="csc">

<xsl:output method="xml" encoding="ISO-8859-1" />

<xsl:param name="g_datetime" required="yes" />

<xsl:template match="//eventdefs">
    <xsl:call-template name="atom">
        <xsl:with-param name="title">Events</xsl:with-param>
        <xsl:with-param name="feedfile">events.atom</xsl:with-param>
        <xsl:with-param name="uuid">006f667f-ecae-483a-8cbc-f0c5084dccb1</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="//newsdefs">
    <xsl:call-template name="atom">
        <xsl:with-param name="title">News</xsl:with-param>
        <xsl:with-param name="feedfile">news.atom</xsl:with-param>
        <xsl:with-param name="uuid">bdae3752-0cb7-4932-b176-019076389b54</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template name="atom">
    <xsl:param name="title"/>
    <xsl:param name="feedfile"/>
    <xsl:param name="uuid"/>
    <feed>
        <title>UW Computer Science Club <xsl:value-of select="$title"/></title>
        <link href="http://csclub.uwaterloo.ca/{$feedfile}" rel="self"/>
        <link href="http://csclub.uwaterloo.ca/"/>
        <updated><xsl:value-of select="translate($g_datetime,' ','T')"/></updated>
        <id>urn:uuid:<xsl:value-of select="$uuid"/></id>
        <author>
        <name>University of Waterloo Computer Science Club</name>
        </author>
        <xsl:for-each select="newsitem">
            <entry>
                <link href="http://csclub.uwaterloo.ca"/>
		<title><xsl:value-of select="."/></title>
                <summary><xsl:value-of select="."/></summary>
                <author><name><xsl:value-of select="@author"/></name></author>
		<id>http://csclub.uwaterloo.ca/news/<xsl:value-of select="generate-id(.)"/></id>
		<updated><xsl:value-of select="translate($g_datetime,' ','T')"/></updated>
            </entry>
        </xsl:for-each>
        <xsl:for-each select="eventitem">
            <entry>
                <title><xsl:value-of select="@title"/></title>
                <!-- we do a hack here with the translate to get whitespace replacement -->
                <link href="{concat('http://csclub.uwaterloo.ca/events/',translate(@room,' ','_'),'-',translate(@date,' ','_'),'-',translate(@time,' ','_'))}"/>
                <author><name><xsl:value-of select="short"/></name></author>
                <summary><xsl:value-of select="abstract"/></summary>
		<updated><xsl:value-of select="translate($g_datetime,' ','T')"/></updated>
		<id><xsl:value-of select="concat('http://csclub.uwaterloo.ca/events/',translate(@room,' ','_'),'-',translate(@date,' ','_'),'-',translate(@time,' ','_'))"/></id>
            </entry>
        </xsl:for-each>
    </feed>
</xsl:template>

</xsl:stylesheet>
