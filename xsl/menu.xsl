<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="menuitem">
  <xsl:variable name="preabs">
    <xsl:choose>
      <xsl:when test="@absolute = 'true'" />
      <xsl:otherwise>
        <xsl:value-of select="$g_pre" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <div class="menuitem">
    <a href="{$preabs}{@href}"><xsl:value-of select="@title" /></a>
  </div>
</xsl:template>

<xsl:template match="menudefs">
  <div class="menubar">
    <xsl:apply-templates select="menuitem" />
  </div>
</xsl:template>

<xsl:template match="menu" name="menu">
 <xsl:apply-templates select="document(concat($g_root, '/menu.xml'))/menudefs" />
</xsl:template>

</xsl:stylesheet>
