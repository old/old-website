<?xml version='1.0'?>

<xsl:stylesheet version="1.1"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:csc="http://csclub.uwaterloo.ca/xsltproc"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="csc">
<xsl:output method="xml" />

<xsl:template match="positions">
  <xsl:variable name="exec" select="@exec" />
  <xsl:for-each select="csc:position-list('')/position[@exec=$exec]">
    <xsl:sort select="@order" order="ascending" />
    <xsl:if test="holder">
      <p>
          <a class="position" href="mailto:{@position}@csclub.uwaterloo.ca"><xsl:value-of select="@name" /></a><br />
        <xsl:for-each select="holder">
          <xsl:value-of select="@name" /><br />
        </xsl:for-each>
      </p>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
