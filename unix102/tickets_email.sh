#!/bin/bash
# Sends email when Beta tickets are available.

TEST=`mktemp`
wget -q --no-check-certificate https://ticketdriver.com/betawater/buy/tickets/ -O ${TEST}

LINEMATCH=`mktemp`
egrep '2012-04-13|Laidback|Luke' ${TEST} >> ${LINEMATCH}

if [ $? = 0 ]; then
   SUBJECT="BUY BETA TICKETS"
   EMAIL=$1
   EMAILBODY=`mktemp`
   NUMBER=$2

   echo "tickets_email: Tickets available!!
https://ticketdriver.com/betawater/buy/tickets/" >> "${EMAILBODY}"
   echo -e "\nMatching line: " >> "${EMAILBODY}"
   cat "${LINEMATCH}" >> "${EMAILBODY}"
   
   mail -s "${SUBJECT}" "${EMAIL}" < "${EMAILBODY}"
   if [ $NUMBER -n "" ]; then
      echo "go buy tickets" | mail -s "${SUBJECT}" "${NUMBER}@msg.telus.com"
   fi
   rm "${EMAILBODY}"
fi

rm "${TEST}" "${LINEMATCH}"
