#!/bin/bash

BASE=`basename $1 .c`

gcc -Wall $1 -o $BASE

for TEST in $BASE.in.*
do
    OUT=`echo -n $TEST | sed -e s/in/out/`
    ./$BASE < $TEST > $OUT
done
